import React, { Component } from 'react';
import './App.css';
import Capital from './Componentes/Capital';
import Marco from './Componentes/Marco';
import FotoBola from './Componentes/FotoBola';
import Contador from './Componentes/Contador';
import Calculadora from './Componentes/Calculadora'

class App extends Component {
  render() {
    return (
      <div>        
      {/* <Capital nom="barcelona" />
      <Capital nom="tarragona" />
      <Capital nom="girona" />
      <br />
      <Marco src="http://lorempixel.com/200/300" borde="10px" color="brown" fondo="beige" />
      <FotoBola src="http://lorempixel.com/200/200" talla="200" />
      <Contador valorInicial="5"/> */}
      <Calculadora />
      
      </div>
    );
  }
}

export default App;
