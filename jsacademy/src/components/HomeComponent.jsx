import React from "react";
import './css/home.css'
import { AnimateKeyframes } from 'react-simple-animate';


export default () => (
    <>

        <div className="rotatingImage">
            <AnimateKeyframes
                play
                iterationCount="infinite"
                durationSeconds={10}
                playState="running"
                keyframes={[
                    'transform: rotateZ(0deg)',
                    'transform: rotateZ(180deg)',
                ]}
            >
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/245px-React.svg.png" />

            </AnimateKeyframes>
        </div>
        <h1 className="title">React Academy</h1>
    </>
);
