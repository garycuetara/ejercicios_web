import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
} from 'reactstrap';
import { Button } from 'reactstrap';
import './components/css/home.css';

//importamos los componentes
import AlumnosComponent from './components/AlumnosComponent'
import ListaAlumnosComponent from './components/ListaAlumnosComponent'
import CursosComponent from './components/CursosComponent'
import ListaCursosComponent from './components/ListaCursosComponent'
import HomeComponent from './components/HomeComponent'
import NuevoAlumnoComponent from './components/NuevoAlumnoComponent'
import NuevoCursoComponent from './components/NuevoCursoComponent'

//importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import EliminaAlumnoComponent from "./components/EliminaAlumnoComponent";

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      alumnos: [],
      cursos: [],
      ultimoAlumno: 0,
      ultimoCurso: 0
    }

    this.cargaDades = this.cargaDades.bind(this);

    this.guardaCurso = this.guardaCurso.bind(this);
    this.eliminaCurso = this.eliminaAlumno.bind(this);

    this.guardaAlumnos = this.guardaAlumnos.bind(this);
    this.eliminaAlumno = this.eliminaAlumno.bind(this);

    this.saveData = this.saveData.bind(this);
    this.loadData = this.loadData.bind(this);

    this.cargaDades();
  }

  componentWillMount() {
    this.loadData();
  }

  cargaDades() {
    const cursosInicio = [
      new CursosComponent(1, "Curso 150h Javascript Full Stack", "JavaScript"),
      new CursosComponent(2, "Curso 200h Java intermedio", "Java"),
      new CursosComponent(3, "Curso 217h .PHP", "PHP"),
    ];

    const alumnosInicio = [
      new AlumnosComponent(1, "Fernando Martín", "fmartin@gmail.com", "18", "Hombre"),
      new AlumnosComponent(2, "María Pérez", "mperez@gmail.com", "22", "Mujer"),
      new AlumnosComponent(3, "Martín González", "mgonzalez@gmail.com", "34", "Hombre"),
    ];
    this.state = {
      alumnos: alumnosInicio,
      ultimoAlumno: 3,
      cursos: cursosInicio,
      ultimoCurso: 3
    };
  }

  guardaCurso(datos) {
    if (datos.idCurso === 0) {
      datos.idCurso = this.state.ultimoCurso + 1;
      this.setState({ ultimoCurso: datos.idCurso });
    }
    let nuevoCurso = new CursosComponent(datos.idCurso, datos.nombreCurso, datos.lenguaje);

    let nuevaLista = this.state.cursos.filter(el => el.idCurso !== nuevoCurso);

    nuevaLista.push(nuevoCurso);

    this.setState({ cursos: nuevaLista });
  }

  guardaAlumnos(datos) {
    if (datos.idAlumno === 0) {
      datos.idAlumno = this.state.ultimoAlumno + 1;
      this.setState({ ultimoAlumno: datos.idAlumno });
    }
    let nuevoAlumno = new AlumnosComponent(datos.idAlumno, datos.nombreAlumno, datos.email, datos.edad, datos.genero);

    let nuevaLista = this.state.alumnos.filter(el => el.idAlumno !== nuevoAlumno);

    nuevaLista.push(nuevoAlumno);

    this.setState({ alumnos: nuevaLista });
  }

  eliminaCurso(idCursoEliminar) {
    let nuevaLista = this.state.cursos.filter(el => el.idCurso !== idCursoEliminar);
    this.setState({ curso: nuevaLista });
  }

  eliminaAlumno(idAlumnoEliminar) {
    let nuevaLista = this.state.alumnos.filter(el => el.idAlumno !== idAlumnoEliminar);
    this.setState({ alumnos: nuevaLista });
  }

  //extra guardado de datos
  saveData() {
    var jsonData = JSON.stringify(this.state);
    localStorage.setItem("datagenda", jsonData);
  }

  //carga de datos
  loadData() {
    var text = localStorage.getItem("datagenda");
    if (text) {
      var obj = JSON.parse(text);
      this.setState(obj);
    }
  }

  resetData() {
    localStorage.clear();
    this.loadData();
  }

  render() {
    return (
      <BrowserRouter>
        <Navbar light expand="md">
          <NavbarBrand>React Academy</NavbarBrand>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Link className="nav-link" to="/">Inicio</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/cursos">Cursos</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/alumnos">Alumnos</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/nuevocurso">Añadir Curso</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/nuevoalumno">Añadir Alumno</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" onClick={this.saveData}>Guardar Datos</Link>{' '}
            </NavItem>
            <NavItem>
              <Link className="nav-link" onClick={this.resetData}>Recargar Datos</Link>{' '}
            </NavItem>
          </Nav>
        </Navbar>
        <div className="main-body">
          <Switch>
            <Route exact path="/" render={() => <HomeComponent />} />
            <Route path="/cursos" render={() => <ListaCursosComponent cursos={this.state.cursos} />} />
            <Route path="/alumnos" render={() => <ListaAlumnosComponent alumnos={this.state.alumnos} />} />
            <Route path="/nuevocurso" render={() => <NuevoCursoComponent guardaCurso={this.guardaCurso} />} />
            <Route path="/nuevoalumno" render={() => <NuevoAlumnoComponent guardaAlumnos={this.guardaAlumnos} />} />
            <Route path="/elimina/:idAlumno" render={(props) => <EliminaAlumnoComponent alumnos={this.state.alumnos} eliminaAlumno={this.eliminaAlumno} {...props} />} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}