$(document).on("click", "#carga", carga);

function carga() {

    let url = "https://api.citybik.es/v2/networks/bicing";

    let minimo = $("#minimo").val();
    minimo = parseInt(minimo);
    $("#marca").empty();

    $.getJSON(url, function (data) {
        let estaciones = data.network.stations;
        for (let i = 0; i < estaciones.length; i++) {
            let estacionActual = estaciones[i];
            if (estacionActual.free_bikes >= minimo) {
                let fila = $("<tr>");
                let celdaNombre = $("<td>").text(estacionActual.name);
                let celdaDisponibles = $("<td>").text(estacionActual.free_bikes);
                let celdaSlots = $("<td>").text(estacionActual.empty_slots);
                let celdaLat = $("<td>").text(estacionActual.latitude);
                let celdaLong = $("<td>").text(estacionActual.longitude);
                fila.append(celdaNombre, celdaDisponibles, celdaSlots, celdaLat, celdaLong);
                $("#marca").append(fila);
                posiciones.push({
                    latitude: estacionActual.latitude,
                    longitude: estacionActual.longitude
                });
            }
        }
    })
}