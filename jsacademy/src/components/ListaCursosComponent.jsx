import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class ListaCursosComponent extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {

        let filas = this.props.cursos.sort((a, b) => a.idCurso - b.idCurso).map(curso => {
            return (
                <tr key={curso.idCurso}>
                    <td>{curso.idCurso}</td>
                    <td>{curso.nombreCurso}</td>
                    <td>{curso.lenguaje}</td>
                </tr>
            );
        })

        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Lenguaje</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        )
    }
}