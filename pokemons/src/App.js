import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

import Pokemon from './models/Pokemon'
import Inici from './components/Inici';
import Taula from './components/Taula';
import NavMenu from './components/NavMenu';
import Edita from './components/Edita';
import P404 from './components/P404';


export default class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <Container>
          <NavMenu />
          <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />
                <Route exact path="/pokemons" render={() => <Taula model={Pokemon} />} />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
    );
  } 
}
