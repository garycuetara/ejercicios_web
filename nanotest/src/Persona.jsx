import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col
} from 'reactstrap';
import './css/nom.css'
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

export default class Persona extends React.Component {

    <Col sm="3">
        <Card>
            <CardImg top width="100%" src={props.data.picture.large} alt="Card image cap" />
            <CardBody>
                <CardTitle><h1 className="nom">{props.data.name.first} {props.data.name.second}</h1></CardTitle>
                <CardSubtitle>{props.data.name.email}</CardSubtitle>
                <CardText><h5>Location: </h5>
                    {props.data.location.street}
                    {props.data.location.city}
                    {props.data.location.state}
                    {props.data.location.postcode} </CardText>
                <Link to={"/detalle/"+props.data.idpersona}><Button color="primary">Detalles</Button></ Link>
            </CardBody>
        </Card>
    </Col>

}