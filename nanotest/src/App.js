import React from "react";
import Thumbs from './Thumbs';
import Tricolor from './Tricolor';
import Fotos from './Fotos';
import Gente from './Gente';

// import Combo from './Combo';
// import { CIUTATS_CAT_20K } from './Datos';
import Tiempo from './Tiempo';

import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem
} from 'reactstrap';



export default () => (
  <BrowserRouter>
    <Navbar color="light" light expand="md">
      <NavbarBrand>Ejercicios React con Enrutamiento</NavbarBrand>     
        <Nav className="ml-auto" navbar>
          <NavItem>
            <Link className="nav-link" to="/thumbs">Thumbs</Link><br />
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/tricolor">Tricolor</Link><br />
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/fotos">Fotos</Link><br />
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/tiempo">Tiempo</Link><br />
          </NavItem>
          <NavItem>
            <Link className="nav-link" to="/gente">Gente</Link><br />
          </NavItem>
        </Nav>
    </Navbar>
    <Switch>
      <Route path="/thumbs" component={Thumbs} />
      <Route path="/tricolor" render={() => <Tricolor colorInicial="grey" />} />
      <Route path="/fotos" component={Fotos} />
      <Route path="/tiempo" component={Tiempo} />
      <Route path="/gente" component={Gente} />
    </Switch>


  </BrowserRouter>

);
