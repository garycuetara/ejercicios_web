import React from 'react';

export default class Boton extends React.Component {
        render(){
            return(
                <button onClick={this.props.clicado}>{this.props.texto}</button>
            );
        }
}