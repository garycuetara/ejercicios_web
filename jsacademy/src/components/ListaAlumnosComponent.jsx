import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class ListaAlumnosComponent extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {

        let filas = this.props.alumnos.sort((a, b) => a.idAlumno - b.idAlumno).map(alumno => {
            return (
                <tr key={alumno.idAlumno}>
                    <td>{alumno.idAlumno}</td>
                    <td>{alumno.nombreAlumno}</td>
                    <td>{alumno.email}</td>
                    <td>{alumno.edad}</td>
                    <td>{alumno.genero}</td>
                    
                </tr>
            );
        })

        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Edad</th>
                        <th>Género</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        )
    }
}