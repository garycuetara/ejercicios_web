import React from 'react';

function Marco(props){
    return <div style={
        {backgroundColor: props.fondo,
        padding: props.borde,
        borderSize: "3px",
        borderColor: props.color,
        borderStyle: "solid",
        display: "inline-block"}
    }>
    <img src={props.src} alt=""/></div>
}

export default Marco;