export default class CursosComponent {
    constructor(idCurso, nombreCurso, lenguaje) {
        this.idCurso = idCurso;
        this.nombreCurso = nombreCurso;
        this.lenguaje = lenguaje;
    }
}
