
 
export default class AlumnosComponent {
    constructor(idAlumno, nombreAlumno, email, edad, genero) {
        this.idAlumno = idAlumno;
        this.nombreAlumno = nombreAlumno;
        this.email = email;
        this.edad = edad;
        this.genero = genero;
    }
}