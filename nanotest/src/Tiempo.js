import React from 'react';
import './css/tiempo.css'

export default class Previsio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }


        let weekday = new Array(7);
        weekday[0] = "Sun";
        weekday[1] = "Mon";
        weekday[2] = "Tues";
        weekday[3] = "Wed";
        weekday[4] = "Thurs";
        weekday[5] = "Fri";
        weekday[6] = "Sat";

        let fecha1 = new Date(this.state.list[0].dt * 1000);
        let fecha2 = new Date(this.state.list[8].dt * 1000);
        let fecha3 = new Date(this.state.list[16].dt * 1000);
        let fecha4 = new Date(this.state.list[24].dt * 1000);
        let fecha5 = new Date(this.state.list[32].dt * 1000);

        let dia1 = weekday[fecha1.getDay()];
        let dia2 = weekday[fecha2.getDay(fecha2)];
        let dia3 = weekday[fecha3.getDay(fecha3)];
        let dia4 = weekday[fecha4.getDay(fecha4)];
        let dia5 = weekday[fecha5.getDay(fecha5)];

        let icon1 = this.state.list[0].weather[0].icon;
        let icon2 = this.state.list[8].weather[0].icon;
        let icon3 = this.state.list[16].weather[0].icon;
        let icon4 = this.state.list[24].weather[0].icon;
        let icon5 = this.state.list[32].weather[0].icon;

        icon1 = "http://openweathermap.org/img/w/" + icon1 + ".png";
        icon2 = "http://openweathermap.org/img/w/" + icon2 + ".png";
        icon3 = "http://openweathermap.org/img/w/" + icon3 + ".png";
        icon4 = "http://openweathermap.org/img/w/" + icon4 + ".png";
        icon5 = "http://openweathermap.org/img/w/" + icon5 + ".png";



        return (
            <div>
                <div className="cardTemps">

                    <h1>{dia1}</h1>
                    <img src={icon1} ></img>
                    <div>

                        < h2> {Math.round(this.state.list[0].main.temp_min)}º</ h2>
                        < h2>{Math.round(this.state.list[0].main.temp_max)}º</ h2>
                    </div>
                </div>
                <div className="cardTemps">

                    <h1>{dia2}</h1>
                    <img src={icon2} ></img>
                    <div>

                        < h2>  {Math.round(this.state.list[8].main.temp_min)}º</ h2>
                        < h2>  {Math.round(this.state.list[8].main.temp_max)}º</ h2>
                    </div>
                </div>
                <div className="cardTemps">

                    <h1>{dia3}</h1>
                    <img src={icon3} ></img>
                    <div>

                        < h2>  {Math.round(this.state.list[16].main.temp_min)}º</ h2>
                        < h2>  {Math.round(this.state.list[16].main.temp_max)}º</ h2>
                    </div>
                </div>
                <div className="cardTemps">

                    <h1>{dia4}</h1>
                    <img src={icon4} ></img>
                    <div>

                        < h2>  {Math.round(this.state.list[24].main.temp_min)}º</ h2>
                        < h2>  {Math.round(this.state.list[24].main.temp_max)}º</ h2>
                    </div>
                </div>
                <div className="cardTemps">

                    <h1>{dia5}</h1>
                    <img src={icon5} ></img>
                    <div>
                        < h2>  {Math.round(this.state.list[32].main.temp_min)}º</ h2>
                        < h2>  {Math.round(this.state.list[32].main.temp_max)}º</ h2>
                    </div>
                </div>
            </div>
        );
    }
}
