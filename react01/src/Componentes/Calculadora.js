import React from 'react';
import './css/Calculadora.css'

export default class Teclado extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            display: "0",
            valorAnterior: 0,
            operacionAnterior: "",
            vaciarDisplay: true
        }
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar(caracter) {
        switch (caracter) {
            case "+":
                this.setState({
                    valorAnterior: this.state.display,
                    operacionAnterior: "+",
                    dislay: "0"
                })
                break;
            case "-":
                this.setState({
                    valorAnterior: this.state.display,
                    operacionAnterior: "-",
                    display: "0"
                })
                break;
            case "*":
                this.setState({
                    valorAnterior: this.state.display,
                    operacionAnterior: "*",
                    display: "0"
                })
                break;
            case "/":
                this.setState({
                    valorAnterior: this.state.display,
                    operacionAnterior: "/",
                    display: "0"
                });
                break;
            case "C":
                this.setState({
                    operacionAnterior: "",
                    display: "0"
                });
                break;
            case "=":
                let resultado = 0;
                switch (this.state.operacionAnterior) {
                    case "+":
                        resultado = parseInt(this.state.display) + parseInt(this.state.valorAnterior);
                        break;
                    case "-":
                        resultado = parseInt(this.state.valorAnterior) - parseInt(this.state.display);
                        break;
                    case "*":
                        resultado = parseInt(this.state.valorAnterior) * parseInt(this.state.display);
                        break;
                    case "/":
                        resultado = parseInt(this.state.valorAnterior) / parseInt(this.state.display);
                        break;
                }
                this.setState({
                    display: resultado,
                    vaciarDisplay: true
                });
                break;
            default:
                if (this.state.vaciarDisplay === true) {
                    this.setState({
                        display: caracter,
                        vaciarDisplay: false
                    });
                } else {
                    this.setState({
                        display: "" + this.state.display + caracter
                    });
                }
                break;
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="calculadoraBody">
                    <h3 className="display">{this.state.display}</h3>
                    <div>
                        <button onClick={() => this.pulsar(1)} >1</button>
                        <button onClick={() => this.pulsar(2)} >2</button>
                        <button onClick={() => this.pulsar(3)} >3</button>
                        <button onClick={() => this.pulsar("C")} >C</button>
                    </div>

                    <div>
                        <button onClick={() => this.pulsar(4)} >4</button>
                        <button onClick={() => this.pulsar(5)} >5</button>
                        <button onClick={() => this.pulsar(6)} >6</button>
                        <button onClick={() => this.pulsar("*")} >*</button>
                    </div>
                    <div>
                        <button onClick={() => this.pulsar(7)} >7</button>
                        <button onClick={() => this.pulsar(8)} >8</button>
                        <button onClick={() => this.pulsar(9)} >9</button>
                        <button onClick={() => this.pulsar("/")} >/</button>
                    </div>
                    <div>
                        <button onClick={() => this.pulsar("+")} >+</button>
                        <button onClick={() => this.pulsar("-")} >-</button>
                        <button onClick={() => this.pulsar(0)} >0</button>
                        <button onClick={() => this.pulsar("=")} >=</button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

