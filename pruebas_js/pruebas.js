function bucle01() {
    num = prompt("entra numero") * 1;
    var total = num;
    var contador = 1;
    var maxim = num;
    var minim = num;
    while (num !== 0) {
        num = prompt("entra numero") * 1;
        total = total + num;
        if (num !== 0) {
            if (num > maxim) {
                maxim = num;
            }
            if (num < minim) {
                minim = num;
            }
            contador++;
        }
    }
    console.log("Total = " + total);
    console.log("Media = " + (total / contador));
    console.log("Minim = " + minim);
    console.log("Maxim = " + maxim);
}

//completado
function mayor(a, b) {
    var mayor = 0;

    if (a > b) {
        console.log("El número mayor es " + a);
    } else if (a < b) {
        console.log("El número mayor es " + b);
    } else {
        console.log("Los dos números son iguales");
    }
}

//completado
function datos(x) {
    if (x % 2 === 0) {
        console.log(x + " es par.");
    } else {
        console.log(x + " es impar.");
    }

    if (x % 3 === 0) {
        console.log("SI es divisible por 3.");
    } else {
        console.log("NO es divisible por 3.");
    }

    if (x % 5 === 0) {
        console.log("SI es divisible por 5.");
    } else {
        console.log("NO es divisible por 5.");
    }

    if (x % 7 === 0) {
        console.log("SI es divisible por 7");
    } else {
        console.log("NO es divisible por 7.")
    }
}

//completado
function sumaValores(arr) {
    var total = 0;

    for (var i = 0; i < arr.length; i++) {
        total += arr[i];
    }
    console.log(total);
}

//completo
function factorial(x) {
    if (x === 0 || x === 1)
        return 1;
    for (var i = x - 1; i >= 1; i--) {
        x *= i;
    }
    return x;

}

//completado
function primo(value) {
    for (var i = 2; i < value; i++) {
        if (value % i === 0) {
            console.log(value + " no es primo");
            return;
        }
    }
    console.log(value + " es primo");
    return;
}

//completado
function fibonacci(x) {
    var array = [0, 1];
    for (var i = 2; i < x; i++) {
        array[i] = array[i - 2] + array[i - 1];
    }
    console.log(array);
}

//completado
function capitaliza(x) {
    return x.charAt(0).toUpperCase() + x.substring(1).toLowerCase();
}

//completado
function palabra(x) {
    var numLetras = x.length;
    var vocales = 0;
    var consonantes = 0;
    console.log("Longitud de texto:");
    console.log("Palabra = " + numLetras);

    if (numLetras % 2 === 0) {
        console.log(numLetras + " es un número par.");
    } else {
        console.log(numLetras + " es un número impar.");
    }

    x.split('');
    for (var i = 0; i < x.length; i++) {
        if (x[i] === 'a' || x[i] === 'e' || x[i] === 'i' || x[i] === 'o' || x[i] === 'u') {
            vocales++;
        } else {
            consonantes++;
        }
    }

    console.log("vocales: " + vocales);
    console.log("consonantes: " + consonantes);
}

//completado
function hoy() {
    var hoy = new Date();

    var diaSemana = hoy.getDay();
    var diasSemana = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]

    console.log("Hoy es " + diasSemana[diaSemana]);
}


//completado
function navidad() {
    var hoy = new Date()
    var navidad = new Date(2019, 12, 25);
    var msPorDia = 24 * 60 * 60 * 1000;
    var tiempoRestante = (navidad.getTime() - hoy.getTime());

    diasRestantesDecimal = tiempoRestante / msPorDia;
    diasRestantes = Math.floor(diasRestantesDecimal);

    console.log("Faltan " + diasRestantes + " días para Navidad.")

}


//completado
function analiza(array) {
    var total = 0;
    var max = array[0];
    var min = array[0];

    for (var i = 0; i < array.length; i++) {
        total += array[i];

        if (array[i] > max) {
            max = array[i];
        }

        if (array[i < min]) {
            min = array[i];
        }
    }
    console.log("La suma de los " + array.length + " es " + total);
    console.log("El número mayor es " + max);
    console.log("El número menor es " + min);
}

function adivina() {
    var numero = Math.floor(Math.random() * 101);
    var contador = 0;
    var acertado = false;
    do {
        var respuesta = prompt("Introduce un número de 1 a 100");

        if (respuesta > numero) {
            console.log("Demasiado alto, vuelve a intentar...")
            contador++;
        } else if (respuesta < numero) {
            console.log("Demasiado bajo, vuelve a intentar...")
            contador++;
        } else {
            console.log("¡Has acertado!")
            console.log("Lo has intentado "+contador+" veces.")
            acertado = true;
        }
    } while (!acertado)
}

function todosPrimos(x){
    var guardar = [];
    var primos = [];
    for (var i = 2; i <= x; i++){
        if(!guardar[i]){
            primos.push(i);
            for (j = i << 1; j <= x; j += i){
                guardar[j] = true;
            }
        }
    }
    return primos;
}