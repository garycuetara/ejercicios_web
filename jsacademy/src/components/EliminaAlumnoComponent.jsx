
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button } from 'reactstrap';

export default class EliminaAlumnoComponent extends Component {
    constructor(props) {
        super(props);
        let id = this.props.match.params.idAlumno * 1;
        let actual = this.props.alumnos.filter(el => el.id === id)[0];

        this.state = {
            nombreAlumno: actual.nombreAlumno,
            email: actual.email,
            idAlumno: actual.idAlumno,
            volver: false
        };

        this.eliminar = this.eleiminar.bind(this);
        this.volver = this.volver.bind(this);

    }

    eliminar(){
        this.props.eliminaAlumno(this.state.idAlumno);
        this.setState({volver: true});
    }

    volver(){
        this.setState({volver: true});
    }

    render() {
       
        if (this.state.volver === true) {
            return <Redirect to='/home' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.nombreAlumno}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.volver} color="success">No</Button>
            </>
        );
    }
}



