import React, { Component } from 'react';

class Fotos extends Component {
    constructor(props) {
        super(props);
        this.state = { foto: "nada" };
        this.selecciona = this.selecciona.bind(this);
    }

    selecciona(evento){
        let valorSeleccionado = evento.target.value;
        this.setState({foto: valorSeleccionado});
    }


    render() { 
        let opciones = [];
        for(let i =0; i<4; i++){
            opciones.push(<option key={i}>{this.state.foto}</option>);
        }

        return ( 
            <>
                <h1>Seleccion: {this.state.foto}</h1>
                <select onChange={this.selecciona}>
                    <option value="coche">Coche</option>
                    <option value="moto">Moto</option>
                    <option value="bici">Bici</option>
                    <option value="bus">Bus</option>
                </select>      

            </>

         );
    }
}
 
export default Fotos;