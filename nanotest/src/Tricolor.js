import React from 'react';
import './css/tricolor.css';

export default class Tricolor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: this.props.colorInicial
        }
        this.pulsar = this.pulsar.bind(this)
    }

    pulsar() {
        switch (this.state.color) {
            case "grey":
                this.setState({
                    color: "red"
                })
                break;
            case "red":
                this.setState({
                    color: "blue"
                })
                break;
            case "blue":
                this.setState({
                    color: "green"
                })
                break;
            case "green":
                this.setState({
                    color: "grey"
                })
                break;
        }
    }

    render() {
        let claseColor = this.state.color;
        return (
            <div className="circle">
                <div className={claseColor} onClick={this.pulsar} />
            </div>
        )
    }

}