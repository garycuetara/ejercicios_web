import React, { Component } from 'react';
import Dato from './Dato';
import Boton from './Boton'

class Contador extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: parseInt(this.props.valorInicial)
        };

        this.displayMas = this.displayMas.bind(this);
        this.displayMenos = this.displayMenos.bind(this);
    }

    displayMenos() {
        if (this.state.display > 0) {

            this.setState({
                display: this.state.display - 1
            })
        }
    }

    displayMas() {
        if (this.state.display < 8){

            this.setState({
                display: this.state.display + 1
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <Boton clicado={this.displayMenos} texto="Menos" />
                <Dato valor={this.state.display} />
                <Dato valor={this.state.display + 1} />
                <Dato valor={this.state.display + 2} />
                <Boton clicado={this.displayMas} texto="Mas" />
            </React.Fragment>
        );
    }
}


export default Contador;