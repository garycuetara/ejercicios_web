import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col
} from 'reactstrap';
import './css/nom.css'

export default class Gente extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            persones: []
        };

        this.getGente = this.getGente.bind(this);
        this.getGente();       

    }

    getGente() {
        const apiUrl = "https://randomuser.me/api/?results=10"


        fetch(apiUrl)
            .then(response => response.json())
            .then(persones => this.setState({persones: persones.results}))
            .catch(error => console.log(error));

            

    }

    render() {
        if (!this.state.persones.length) {
            return <h1>Cargando datos...</h1>
        }
        
        let gent = this.state.persones.map(el => {
            return(
                {nom: el.name.first,
                cognom: el.name.last,
                email: el.email,
                street: el.location.street,
                city: el.location.city,
                state: el.location.state,
                postcode: el.location.postcode,
                picture: el.picture.large}
            )
        })

        let identificador = 0;

        let cards = gent.map(el => {
            return (
                <Col sm="3" key={identificador++}>
                        <Card>
                            <CardImg top width="100%" src={el.picture} alt="Card image cap" />
                            <CardBody>
                                <CardTitle><h1 className="nom">{el.nom} {el.cognom}</h1></CardTitle>
                                <CardSubtitle>{el.email}</CardSubtitle>
                                <CardText><h5>Location: </h5>
                                    {el.street} 
                                    {el.city} 
                                    {el.state} 
                                    {el.postcode} </CardText>
                                <Button color="primary">Detalles</Button>
                            </CardBody>
                        </Card>
                    </Col>
                )
        })
        

        return (
            <div>
                <Row>
                    {cards}
                </Row>

            </div>
        )
    }
}
