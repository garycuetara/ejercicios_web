
import React from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


export default class NuevoAlumnoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nombreAlumno: '',
            email: '',
            edad: '',
            genero: 'Hombre',
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submit(e) {
        e.preventDefault();
        this.props.guardaAlumnos({
            nombreAlumno: this.state.nombreAlumno,
            email: this.state.email,
            edad: this.state.edad,
            genero: this.state.genero,
            idAlumno: 0
        });
        this.setState({ volver: true });
    }

    render() {
        if (this.state.volver === true) {
            return <Redirect to='/alumnos' />
        }

        return (
            <div className="main-form">
                <Form onSubmit={this.submit}>
                    <Row>
                        <Col xs="6">
                            <FormGroup>
                                <Label for="nombreInput">Nombre</Label>
                                <Input type="text"
                                    name="nombreAlumno"
                                    id="nombreInput"
                                    value={this.state.nombreAlumno}
                                    onChange={this.cambioInput} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="emailInput">E-mail</Label>
                                <Input type="text" name="email" id="emailInput"
                                    value={this.state.email}
                                    onChange={this.cambioInput} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="3">
                            <FormGroup>
                                <Label for="edadInput">Edad</Label>
                                <Input type="text" name="edad" id="edadInput"
                                    value={this.state.edad}
                                    onChange={this.cambioInput} />
                            </FormGroup>
                        </Col>
                        <Col xs="3">
                            <FormGroup>
                                <Label for="generoInput">Genero</Label>
                                <Input type="select" name="genero" id="generoInput"
                                    value={this.state.genero}
                                    onChange={this.cambioInput}>
                                    <option>Hombre</option>
                                    <option>Mujer</option>
                                    <option>Otro</option></Input>
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <Button color="primary">Guardar</Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}