import React from 'react';
import './css/Capital.css';

function Capital(props){
    let primeraLetra = props.nom.substring(0,1);
    let strResta = props.nom.substring(1);
    primeraLetra = primeraLetra.toUpperCase();
    return <div className="capital"> <span>{primeraLetra}</span> <p>{primeraLetra}{strResta}</p> </div>
}

export default Capital;