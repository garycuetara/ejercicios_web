import React from 'react';
import { Redirect } from 'react-router-dom'

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


export default class NuevoCursoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            nombreCurso: '',
            lenguaje: '',
            volver: false
         }

         this.cambioInput = this.cambioInput.bind(this);
         this.submit = this.submit.bind(this);
    }

    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked: target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submit(e) {
        e.preventDefault();
        this.props.guardaCurso({
            nombreCurso: this.state.nombreCurso,
            lenguaje: this.state.lenguaje,
            idCurso: 0
        });
        this.setState({volver: true});
    }
    render() { 
        if (this.state.volver === true){
            return <Redirect to='/cursos' />
        }


        return ( 

            <Form onSubmit={this.submit}>
            <Row>
                <Col xs="6">
                <FormGroup>
                    <Label for="nombreInput">Nombre del Curso</Label>
                    <Input type="text"
                        name="nombreCurso"
                        id="nombreInput"
                        value={this.state.nombreCurso}
                        onChange={this.cambioInput} />
                </FormGroup>
                <FormGroup>
                    <Label for="lenguajeInput">Especialidad</Label>
                    <Input type="text" name="lenguaje" id="lenguajeInput"
                        value={this.state.lenguaje}
                        onChange={this.cambioInput} />
                </FormGroup>

                </Col>
            </Row>

            <Row>
                <Col>
                    <Button color="primary">Guardar</Button>
                </Col>
            </Row>
            </Form>
         );
    }
}